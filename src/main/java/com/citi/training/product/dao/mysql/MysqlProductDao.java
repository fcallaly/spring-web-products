package com.citi.training.product.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.exceptions.ProductNotFoundException;
import com.citi.training.product.model.Product;

@Component
public class MysqlProductDao implements ProductDao {
    private static String FIND_ALL_SQL = "select id, name, price from product";
    private static String FIND_SQL = "select id, name, price from product where id = ?";
    private static String INSERT_SQL = "insert into product (name, price) values (?, ?)";
    private static String DELETE_SQL = "delete from product where id=?";

    @Autowired
    JdbcTemplate tpl;

    public List<Product> findAll(){
        return tpl.query(FIND_ALL_SQL,
                         new ProductMapper());
    }

    @Override
    public Product findById(int id) {
        List<Product> Products = this.tpl.query(FIND_SQL,
                new Object[]{id},
                new ProductMapper()
        );
        if(Products.size() <= 0) {
            throw new ProductNotFoundException("No product found with id: " +
                                               id);
        }
        return Products.get(0);
    }

    @Override
    public int create(Product Product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                            connection.prepareStatement(INSERT_SQL,
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, Product.getName());
                    ps.setDouble(2, Product.getPrice());
                    return ps;
                }
            },
            keyHolder);
        return keyHolder.getKey().intValue();
    }

    @Override
    public void deleteById(int id) {
        if(this.tpl.update(DELETE_SQL, id) <= 0) {
            throw new ProductNotFoundException("No product found with id: " + id);
        }
    }

    private static final class ProductMapper implements RowMapper<Product> {
        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Product(rs.getInt("id"),
                                rs.getString("name"),
                                rs.getDouble("price"));
        }
    }
}
