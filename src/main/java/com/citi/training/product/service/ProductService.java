package com.citi.training.product.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.model.Product;

@Component
public class ProductService {

    private static final Logger logger =
                LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductDao productDao;

    public int create(Product product) {
        logger.debug("ProductService create: " + product);
        return productDao.create(product);
    }
}
