package com.citi.training.product.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {

    @MockBean
    ProductDao mockProductDao;

    @Autowired
    ProductService productService;

    @Test
    public void test_create() {
        int expectedId = 1;

        when(mockProductDao.create(any(Product.class))).thenReturn(1);

        int newId = productService.create(new Product(-1, "Bob", 29.99));

        assertEquals(newId, expectedId);
    }
}
